package com.haxor.bookmyevents;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Toast;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.dexafree.materialList.card.OnActionClickListener;
import com.dexafree.materialList.card.action.TextViewAction;
import com.dexafree.materialList.view.MaterialListView;
import com.haxor.bookmyevents.SQLite.SQLiteDBHelper;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarTab;
import com.roughike.bottombar.OnTabClickListener;
import com.squareup.picasso.RequestCreator;

import java.util.ArrayList;

public class MainEventsActivity extends Activity {
    private Context mContext;
    private BottomBar bottomBar;
    public MaterialListView mListView;

    private String userEmail;
    public ArrayList<Card> cardArrayList;
    SQLiteDBHelper sqLiteDBHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_events);

        mContext = this;

        SharedPreferences sharedPreferences = getSharedPreferences("bme_login", Context.MODE_PRIVATE);
        if(sharedPreferences.getBoolean("isLoggedIn", false)) {
            userEmail = sharedPreferences.getString("email", null);
        }   else {
            finish();
        }

        cardArrayList = new ArrayList<>();
        mListView = (MaterialListView) findViewById(R.id.material_listview);

        sqLiteDBHelper = new SQLiteDBHelper(this, MainEventsActivity.this);

        if(isOnline()) {
            sqLiteDBHelper.pullEventArray();
        } else {
            sqLiteDBHelper.fetchArrayFromDB();
        }

        bottomBar = BottomBar.attachShy((CoordinatorLayout) findViewById(R.id.myCoordinator),
                findViewById(R.id.material_listview), savedInstanceState);
        bottomBar.setItems(
                new BottomBarTab(R.drawable.ic_home, "Events"),
                new BottomBarTab(R.drawable.ic_search, "Search"),
                new BottomBarTab(R.drawable.ic_user_menu, "User"),
                new BottomBarTab(R.drawable.ic_settings, "Settings"),
                new BottomBarTab(R.drawable.ic_notification, "Notifications")
        );

        bottomBar.setOnTabClickListener(new OnTabClickListener() {
            @Override
            public void onTabSelected(@IdRes int menuItemId) {
                Toast.makeText(mContext, getMessage(menuItemId, false), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onTabReSelected(@IdRes int menuItemId) {
                Toast.makeText(mContext, getMessage(menuItemId, true), Toast.LENGTH_LONG).show();
            }
        });

        bottomBar.mapColorForTab(0, "#607D8B");
        bottomBar.mapColorForTab(1, "#7B1FA2");
        bottomBar.mapColorForTab(2, "#3949AB");
        bottomBar.mapColorForTab(3, "#00897B");
        bottomBar.mapColorForTab(4, "#43A047");

    }

    @Override
    public void onResume(){
        super.onResume();
        bottomBar.selectTabAtPosition(0, true);
    }

    private String getMessage(int menuItemId, boolean isReselection) {
        String message = "Content for ";
        Intent i;

        switch (menuItemId) {
            case 0:
                message += "Events";
                break;
            case 1:
                message += "Search";
                i = new Intent(MainEventsActivity.this, SearchEventsActivity.class);
                startActivity(i);
                break;
            case 2:
                message += "User";
                i = new Intent(MainEventsActivity.this, UserActivity.class);
                startActivity(i);
                break;
            case 3:
                message += "Settings";
                i = new Intent(MainEventsActivity.this, SettingsActivity.class);
                startActivity(i);
                break;
            case 4:
                message += "Notifications";
                i = new Intent(MainEventsActivity.this, NotificationActivity.class);
                startActivity(i);
                break;
        }

        if (isReselection) {
            message += " WAS RESELECTED! YAY!";
        }

        return message;
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public Card generateEventCard(String title, String desc, String imgUrl) {
        Card card = new Card.Builder(this)
                .withProvider(new CardProvider())
                .setLayout(R.layout.material_image_with_buttons_card)
                .setTitle(title)
                .setTitleColor(Color.WHITE)
                .setDescription(desc)
                .setDrawable(imgUrl)
                .setDrawableConfiguration(new CardProvider.OnImageConfigListener() {
                    @Override
                    public void onImageConfigure(@NonNull RequestCreator requestCreator) {
                        requestCreator.centerCrop()
                                .resize(150, 90);
                    }
                })
                .addAction(R.id.left_text_button, new TextViewAction(this)
                        .setText("Details")
                        .setTextResourceColor(R.color.black_button)
                        .setListener(new OnActionClickListener() {
                            @Override
                            public void onActionClicked(View view, Card card) {

                            }
                        }))
                .addAction(R.id.right_text_button, new TextViewAction(this)
                        .setText("Share")
                        .setTextResourceColor(R.color.accent_material_dark)
                        .setListener(new OnActionClickListener() {
                            @Override
                            public void onActionClicked(View view, Card card) {
                                Toast.makeText(mContext, "You have pressed the right button", Toast.LENGTH_SHORT).show();
                            }
                        }))
                .endConfig()
                .build();

        return card;
    }
}
