package com.haxor.bookmyevents.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.haxor.bookmyevents.MainEventsActivity;
import com.haxor.bookmyevents.Models.EventModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SQLiteDBHelper extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "EventsDB";
    private static final String TB_Events = "events";

    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_DESC = "description";
    private static final String KEY_TYPE = "type";
    private static final String KEY_URL = "url";
    private static final String KEY_IMG = "img";
    private static final String KEY_SCHED = "schedule";
    private static final String KEY_DURATION = "duration";
    private static final String KEY_VENUE = "venue";
    private static final String KEY_EXTRA = "extra";

    private Context mContext;
    private MainEventsActivity mainEventsActivity;

    private String jsonUrl = "http://hxworld.xyz/bookmyevents/events/events.json";

    public SQLiteDBHelper(Context context, MainEventsActivity mainEventsActivity) {
        super(context, DB_NAME, null, DB_VERSION);
        mContext = context;
        this.mainEventsActivity = mainEventsActivity;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_Events_TABLE = "CREATE TABLE " + TB_Events + "(" + KEY_ID + " TEXT PRIMARY KEY, "
                + KEY_NAME + " TEXT, " + KEY_DESC + " TEXT, " + KEY_TYPE + " TEXT, "
                + KEY_URL + " TEXT, " + KEY_IMG + " TEXT, " + KEY_SCHED + " TEXT, "
                + KEY_DURATION + " TEXT, " + KEY_VENUE + " TEXT, " + KEY_EXTRA + " TEXT)";
        sqLiteDatabase.execSQL(CREATE_Events_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TB_Events);
        onCreate(sqLiteDatabase);
    }

    public void resetDB() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TB_Events);
        onCreate(db);
    }

    public void addEvent(EventModel event) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_ID, event.getId());
        contentValues.put(KEY_NAME, event.getName());
        contentValues.put(KEY_DESC, event.getDescription());
        contentValues.put(KEY_TYPE, event.getType());
        contentValues.put(KEY_URL, event.getUrl());
        contentValues.put(KEY_IMG, event.getImg());
        contentValues.put(KEY_SCHED, event.getSchedule());
        contentValues.put(KEY_DURATION, event.getDuration());
        contentValues.put(KEY_VENUE, event.getVenue());
        contentValues.put(KEY_EXTRA, event.getExtra());

        db.insert(TB_Events, null, contentValues);
        db.close();
    }

    public void pullEventArray() {
        resetDB();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, jsonUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(final String s) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            JSONArray jsonArray = jsonObject.getJSONArray("events");
                            for(int i = 0; i < jsonArray.length(); i++) {
                                JSONObject eventObject = jsonArray.getJSONObject(i);

                                EventModel eventModel = new EventModel();
                                eventModel.setId(eventObject.getString(KEY_ID));
                                eventModel.setName(eventObject.getString(KEY_NAME));
                                eventModel.setDescription(eventObject.getString(KEY_DESC));
                                eventModel.setType(eventObject.getString(KEY_TYPE));
                                eventModel.setUrl(eventObject.getString(KEY_URL));
                                eventModel.setImg(eventObject.getString(KEY_IMG));
                                eventModel.setSchedule(eventObject.getString(KEY_SCHED));
                                eventModel.setDuration(eventObject.getString(KEY_DURATION));
                                eventModel.setVenue(eventObject.getString(KEY_VENUE));
                                eventModel.setExtra(eventObject.getString(KEY_EXTRA));

                                addEvent(eventModel);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                fetchArrayFromDB();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(mContext).add(stringRequest);
    }

    public void fetchArrayFromDB() {
        final ArrayList<Map<String, String>> dbArrayList = new ArrayList<>();
        String SQLQuery = "SELECT "+ KEY_ID + ", " + KEY_NAME + ", " + KEY_SCHED + ", " + KEY_IMG +" FROM " + TB_Events;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(SQLQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Map<String, String> eventMap = new HashMap<>();
                eventMap.put(KEY_ID, cursor.getString(0));
                eventMap.put(KEY_NAME, cursor.getString(1));
                eventMap.put(KEY_SCHED, cursor.getString(2));
                eventMap.put(KEY_IMG, cursor.getString(3));

                dbArrayList.add(eventMap);
            } while (cursor.moveToNext());
        }
        db.close();

        mainEventsActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (Map<String, String> event : dbArrayList) {

                    String title = event.get(KEY_NAME);
                    String description = event.get(KEY_SCHED);
                    String imgUrl = event.get(KEY_IMG);

                    mainEventsActivity.cardArrayList.add(mainEventsActivity.generateEventCard(title, description, imgUrl));
                }
                mainEventsActivity.mListView.getAdapter().addAll(mainEventsActivity.cardArrayList);
            }
        });
    }

}
