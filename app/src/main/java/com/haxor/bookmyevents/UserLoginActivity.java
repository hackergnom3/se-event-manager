package com.haxor.bookmyevents;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import mehdi.sakout.fancybuttons.FancyButton;

public class UserLoginActivity extends Activity {

    private static final String volleyUrl = "http://hxworld.xyz/bookmyevents/volley_login.php";

    private static final String KEY_EMAIL = "email";
    private static final String KEY_PASSWORD = "password";

    private EditText emailEditText;
    private EditText passwordEditText;
    private FancyButton btn_signin;
    private FancyButton btn_signup;
    private ProgressBar progressBar;

    private String email;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/main_typeface.ttf");

        TextView logoTextView = (TextView) findViewById(R.id.logoTextView);
        logoTextView.setTypeface(typeface);

        emailEditText = (EditText) findViewById(R.id.login_email);
        passwordEditText = (EditText) findViewById(R.id.login_password);

        btn_signup = (FancyButton) findViewById(R.id.signup);
        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserLoginActivity.this, UserSignUpActivity.class);
                UserLoginActivity.this.startActivity(intent);
            }
        });

        btn_signin = (FancyButton) findViewById(R.id.signin);
        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userLogin();
            }
        });
    }

    private void userLogin() {
        email = emailEditText.getText().toString();
        password = passwordEditText.getText().toString();

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, volleyUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        if (s.trim().equals("success")) {
                            SharedPreferences sharedPreferences = getSharedPreferences("bme_login", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putBoolean("isLoggedIn", true);
                            editor.putString(KEY_EMAIL, email);
                            editor.commit();

                            Intent intent = new Intent(UserLoginActivity.this, MainEventsActivity.class);
                            UserLoginActivity.this.startActivity(intent);
                            finish();

                        }   else {
                            Toast.makeText(UserLoginActivity.this, s, Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(UserLoginActivity.this,volleyError.toString(),Toast.LENGTH_LONG ).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put(KEY_EMAIL, email);
                map.put(KEY_PASSWORD, password);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError volleyError) throws VolleyError {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        progressBar.setVisibility(View.INVISIBLE);

    }
}
