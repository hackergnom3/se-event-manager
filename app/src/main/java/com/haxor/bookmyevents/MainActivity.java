package com.haxor.bookmyevents;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import mehdi.sakout.fancybuttons.FancyButton;

public class MainActivity extends AppCompatActivity {

    FancyButton btn_organizer;
    FancyButton btn_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = getSharedPreferences("bme_login", Context.MODE_PRIVATE);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/main_typeface.ttf");

        TextView logoTextView = (TextView) findViewById(R.id.logoTextView);
        logoTextView.setTypeface(typeface);

        btn_organizer = (FancyButton) findViewById(R.id.btn_organizer);
        btn_user = (FancyButton) findViewById(R.id.btn_user);

        if (sharedPreferences.getBoolean("isLoggedIn", false)) {
            Intent i = new Intent(MainActivity.this, MainEventsActivity.class);
            MainActivity.this.startActivity(i);
            finish();
        }

        btn_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, UserLoginActivity.class);
                MainActivity.this.startActivity(intent);
                MainActivity.this.finish();
            }
        });
    }
}
