package com.haxor.bookmyevents;

import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.heinrichreimersoftware.singleinputform.SingleInputFormActivity;
import com.heinrichreimersoftware.singleinputform.steps.Step;
import com.heinrichreimersoftware.singleinputform.steps.TextStep;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserSignUpActivity extends SingleInputFormActivity {

    private static final String DATA_KEY_NAME = "name";
    private static final String DATA_KEY_EMAIL = "email";
    private static final String DATA_KEY_PASSWORD = "password";
    private static final String DATA_KEY_PHNO = "phone";

    private static final String volleyUrl = "http://hxworld.xyz/bookmyevents/volley_signup.php";

    private String name, email, password, phno;

    @Override
    protected List<Step> getSteps(Context context) {
        List<Step> steps = new ArrayList<Step>();

        steps.add(new TextStep(this, DATA_KEY_NAME,
                InputType.TYPE_CLASS_TEXT,
                R.string.name,
                R.string.name_error,
                R.string.name_details,
                new TextStep.StepChecker() {
                    @Override
                    public boolean check(String s) {
                        return !s.equals("");
                    }
                }
        ));

        steps.add(new TextStep(this, DATA_KEY_EMAIL,
                InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS,
                R.string.email,
                R.string.email_error,
                R.string.email_details,
                new TextStep.StepChecker() {
                    @Override
                    public boolean check(String s) {
                        return Patterns.EMAIL_ADDRESS.matcher(s).matches();
                    }
                }
        ));

        steps.add(new TextStep(this, DATA_KEY_PASSWORD,
                InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD,
                R.string.password,
                R.string.password_error,
                R.string.password_details,
                new TextStep.StepChecker() {
                    @Override
                    public boolean check(String s) {
                        return s.length() >= 8;
                    }
                }
        ));

        steps.add(new TextStep(this, DATA_KEY_PHNO,
                InputType.TYPE_CLASS_PHONE,
                R.string.phone,
                R.string.phone_error,
                R.string.phone_details,
                new TextStep.StepChecker() {
                    @Override
                    public boolean check(String s) {
                        return Patterns.PHONE.matcher(s).matches();
                    }
                }
        ));

        return steps;
    }

    @Override
    protected void onFormFinished(Bundle bundle) {
        registerUse(bundle);
        finish();
    }

    private void registerUse(Bundle bundle) {
        email = TextStep.text(bundle, DATA_KEY_EMAIL);
        name = TextStep.text(bundle, DATA_KEY_NAME);
        password = TextStep.text(bundle, DATA_KEY_PASSWORD);
        phno = TextStep.text(bundle, DATA_KEY_PHNO);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, volleyUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        if (!s.trim().equals("success")) {
                            Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Registration successful, please login", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put(DATA_KEY_NAME,name);
                params.put(DATA_KEY_EMAIL,email);
                params.put(DATA_KEY_PASSWORD,password);
                params.put("ph_no",phno);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
