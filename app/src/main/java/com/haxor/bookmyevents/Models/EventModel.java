package com.haxor.bookmyevents.Models;

public class EventModel {
    private String id;
    private String name;
    private String description;
    private String type;
    private String url;
    private String img;
    private String schedule;
    private String duration;
    private String venue;
    private String extra;

    public String getId(){
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getType() {
        return type;
    }

    public String getUrl() {
        return url;
    }

    public String getImg() {
        return img;
    }

    public String getSchedule() {
        return schedule;
    }

    public String getDuration() {
        return duration;
    }

    public String getVenue() {
        return venue;
    }

    public String getExtra() {
        return extra;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }
}
